import sys
NOTES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

data = []
with open(sys.argv[1]) as infile:
    for line in infile:
        line = line.strip()

        # Skip empty lines and comments if any.
        if not line or line.startswith("#"):
            continue

        parts = line.strip().split()

        if not parts[0] == "-":
            n = 1 + NOTES.index(parts[0].upper()) + int(parts[1]) * 12
            duration = int(parts[2])
        else:
            n = 0
            duration = int(parts[1])

        data.append(n)
        data.append(duration)
data.append(255)

print("melody:")

for i in range(0, len(data), 16):
    print(".byt " + ", ".join(map(str, data[i:i+16])))
