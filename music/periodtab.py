# The frequency for a given note.
def note_freq(n):
    return 440.0 * 2**((n-9)/12)

# The period of a given frequency.
def freq_to_period(freq):
    return int(round(1.789773e6 / (16 * freq)) - 1)


# Three octaves' worth.
low = [0]
high = [0]
for n in range(36):
    freq = note_freq(n)
    period = freq_to_period(freq)
    low.append(period & 0xff)
    high.append(period >> 8)

print("Note period table")
print("period_lo: .byt " + ", ".join(map(str, low)))
print("period_hi: .byt " + ", ".join(map(str, high)))
