main:
    ; Enable rendering.
    jsr render_enable

    ; We are still in vblank at this point, hopefully.
    ; Draw text.
    ldx #18
    ldy #13
    jsr goto_tile
    store_word(STRING_ADDRESS, message)
    jsr draw_string

    ; Try reading the joypad.
test_loop:
    jsr read_joypad
    ora #$00
    beq test_loop

    ; Halt
    jmp halt

message:
    .byt "Hello, World!", 0
