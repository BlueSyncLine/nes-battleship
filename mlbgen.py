offset = 0x8000
with open("labels.txt") as labelfile:
    with open("output.mlb", "w") as outfile:
        for line in labelfile:
            parts = line.split(", ")

            name, address = parts[:2]
            address = int(address, 16)
            prg_address = address - offset
            if prg_address >= 0:
                outfile.write("P:{:04x}:{}\n".format(prg_address, name))
            elif address < 2048:
                outfile.write("R:{:04x}:{}\n".format(address, name))
