from sys import argv
CHARS = ".@%#"

pix = []

swap = False
if len(argv) == 3 and argv[2] == "s":
    swap = True

print("Swap", swap)

with open(argv[1], "r") as infile:
    for line in infile:
        pix += [CHARS.index(c) for c in line if c in CHARS]

assert len(pix) == 64

b0 = []
b1 = []
for i in range(8):
    byte0 = 0
    byte1 = 0
    for j in range(8):
        if not swap:
            v = pix[i * 8 + 7 - j]
        else:
            v = pix[j * 8 + i]

        bit0 = v & 1
        bit1 = (v >> 1) & 1

        # Shift in.
        byte0 >>= 1
        byte0 |= bit0 << 7
        byte1 >>= 1
        byte1 |= bit1 << 7

    b0.append(byte0)
    b1.append(byte1)

print(".byt " + ", ".join(["${:02x}".format(b) for b in b0]))
print(".byt " + ", ".join(["${:02x}".format(b) for b in b1]))
