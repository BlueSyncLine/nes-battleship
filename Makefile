BASELIB=baselib
XA=xa -vC -I$(BASELIB)

all: clean output.nes output.mlb

clean:
	rm -f *.bin output.nes output.mlb labels.txt

output.nes: chr-rom.bin prg-rom.bin
	$(XA) $(BASELIB)/ines.asm -o output.nes

chr-rom.bin:
	$(XA) chr-rom.asm -o chr-rom.bin

prg-rom.bin:
	$(XA) $(BASELIB)/prg-rom.asm -l labels.txt -o prg-rom.bin

output.mlb:
	python3 mlbgen.py
