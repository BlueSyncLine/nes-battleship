; NOTE The way the AI algorithm works right now means that aligning the ships vertically would
; give an advantage to the player since the AI only checks that orientation after left-right.

FIELD_ADDRESS = USER_ZPG
CELL_X = FIELD_ADDRESS + 2
CELL_Y = CELL_X + 1
SHIP_LENGTH = CELL_Y + 1
SHIP_VERTICAL = SHIP_LENGTH + 1

; The LFSR is three bytes long.
RANDOM_LFSR = SHIP_VERTICAL + 1

; Game fields.
FIELD_PLAYER = RANDOM_LFSR + 3
FIELD_COMPUTER = FIELD_PLAYER + 100

; Temporary index into the field.
FIELD_INDEX = FIELD_COMPUTER + 100
CURSOR_X = FIELD_INDEX + 1
CURSOR_Y = CURSOR_X + 1

; Address of the ship table.
SHIP_TABLE_ADDRESS = CURSOR_Y + 1
SHIP_INDEX = SHIP_TABLE_ADDRESS + 2

PLAYER_KILLS = SHIP_INDEX + 1
COMPUTER_KILLS = PLAYER_KILLS + 1

; Whose turn is it?
PLAYER_TURN = COMPUTER_KILLS + 1

; Alternative coordinate for the AI.
AI_ALTERNATIVE_COORD = PLAYER_TURN + 1

; Win?
WIN = AI_ALTERNATIVE_COORD + 1
WIN_PLAYER = 1
WIN_COMPUTER = 2

; Previous coordinates chosen by the player.
PLAYER_PREV_X = WIN + 1
PLAYER_PREV_Y = PLAYER_PREV_X + 1

; Music routine.
MELODY_TIMER = PLAYER_PREV_Y + 1
MELODY_INDEX = MELODY_TIMER + 1
MELODY_ENABLED = MELODY_INDEX + 1

; Ship table arrangement:
; X Y vertical
; The lengths are taken from the ship_lengths table.
SHIP_TABLE_PLAYER = $200
SHIP_TABLE_COMPUTER = SHIP_TABLE_PLAYER + 10 * 3

; Types of ship cells (for fancy graphics)
SHIP_CELL_FRONT = 1
SHIP_CELL_MIDDLE = 2
SHIP_CELL_REAR = 3
SHIP_CELL_SINGLE = 4
SHIP_CELL_VERTICAL = 4

; Ship wreckage tile is #11.
SHIP_CELL_WRECKAGE = 11

; The revealed-empty cell is #12.
SHIP_CELL_EMPTY_REVEALED = 12

; Other tile indices.
TILE_UNKNOWN    = 128 + 9
TILE_CURSOR     = 128 + 10
TILE_EXPLOSION  = 128 + 13

; OAM shadow.
OAM_SHADOW_ADDRESS = $700

; Cursor.
CURSOR_SPRITE = OAM_SHADOW_ADDRESS

; Explosion sprites.
EXPLOSION_SPRITE_A = CURSOR_SPRITE + OAM_SPRITE_LEN
EXPLOSION_SPRITE_B = EXPLOSION_SPRITE_A + OAM_SPRITE_LEN
EXPLOSION_SPRITE_C = EXPLOSION_SPRITE_B + OAM_SPRITE_LEN
EXPLOSION_SPRITE_D = EXPLOSION_SPRITE_C + OAM_SPRITE_LEN

; NMI handler.
NMI:
    ; Preserve the accumulator.
    pha

    ; Copy the OAM over.
    lda #0
    sta OAMADDR
    lda #OAM_SHADOW_ADDRESS>>8
    sta OAMDMA

    ; Next step of the melody.
    jsr melody_frame

    ; Restore the accumulator.
    pla

    ; Let the base library handle the interrupt as well.
    jmp default_NMI

; Main.
main:
    ; Initialize the sprite attributes.
    lda #0
    sta CURSOR_SPRITE + OAM_ATTRIBUTES

    lda #1
    sta EXPLOSION_SPRITE_A + OAM_ATTRIBUTES
    sta EXPLOSION_SPRITE_B + OAM_ATTRIBUTES
    sta EXPLOSION_SPRITE_C + OAM_ATTRIBUTES
    sta EXPLOSION_SPRITE_D + OAM_ATTRIBUTES

    ; And the palettes.
    PPU_set_address($3f11)
    lda #$2c
    sta PPUDATA
    lda #$00
    sta PPUDATA
    lda #$00
    sta PPUDATA

    PPU_set_address($3f15)
    lda #$16
    sta PPUDATA
    lda #$27
    sta PPUDATA
    lda #$20
    sta PPUDATA

    ; Initialize the APU.
    lda #$00
    sta APUPULSEA_VOL
    sta APUPULSEA_SWEEP
    sta APUPULSEA_TLOW
    sta APUPULSEA_THIGH
    sta APUTRNG_BASE
    sta APUTRNG_TLOW
    sta APUTRNG_THIGH
    sta APUNOISE_VOL
    sta APUNOISE_PERIOD
    sta APUNOISE_LEN
    lda #APUSTATUS_ENPA|APUSTATUS_ENTRI|APUSTATUS_ENNSE
    sta APUSTATUS

    ; Disable music (if soft-reset).
    jsr melody_disable

    ; Seed the random generator.
    jsr random_seed_prompt

reset_game:
    ; Disable rendering.
    jsr render_disable

    ; Reset the variables.
    lda #0
    sta PLAYER_KILLS
    sta COMPUTER_KILLS
    sta WIN
    sta PLAYER_TURN
    sta PLAYER_PREV_X
    sta PLAYER_PREV_Y

    ; Populate the fields.
    jsr select_player_field
    jsr populate_field
    jsr select_computer_field
    jsr populate_field

    ; Draw the screen and show the cursor.
    jsr draw_screen
    jsr show_cursor

    ; Enable rendering with sprites.
    jsr render_enable_with_spr

    ; Enable music.
    jsr melody_enable

    ; Start AI.
    jmp ai_loop

; Converts the cell coordinate variables to a Y register value for addressing
;  into the game field.
cell_coords:
    ; Save A.
    pha

    ; Load the Y coordinate.
    lda CELL_Y

    ; A = Y * 4
    asl
    asl

    ; A = Y * 4 + Y = Y * 5
    clc
    adc CELL_Y

    ; A = Y * 5 * 2 = Y * 10
    asl

    ; A = Y * 10 + X
    clc
    adc CELL_X

    ; Move A to Yreg
    tay

    ; Restore A.
    pla
    rts

; Checks the coordinates at CELL_X, CELL_Y for validity.
; The carry is clear if the coordinates are not valid.
check_coords:
.(
    ; Save A.
    pha

    ; Check if CELL_X is less than 10 (unsigned).
    sec
    lda #9
    sbc CELL_X
    bcc return

    ; The same check, for CELL_Y.
    sec
    lda #9
    sbc CELL_Y

    ; Regardless of whether the last step cleared the carry or not, we'll
    ;  end up here.
return:
    ; Restore A.
    pla

    rts
.)

; Can a SHIP_LENGTH ship be placed at CELL_X, CELL_Y (orientation SHIP_VERTICAL)?
; Carry clear on failure.
can_place_ship:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; Preserve Y.
    tya
    pha

    ; Preserve the coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Counter.
    ldx SHIP_LENGTH
loop:
    ; Check the target cell.
    jsr check_coords
    bcc fail

    ; Check for a ship.
    jsr cell_coords
    lda (FIELD_ADDRESS), y
    bne fail

    ; Check for neighbors.
    jsr check_neighbors

    ; No carry, fail.
    bcc fail

    ; Vertical orientation?
    lda SHIP_VERTICAL
    bne vertical

    ; No, horizontal.
    inc CELL_X
    jmp next

vertical:
    ; Yes, vertical.
    inc CELL_Y

next:
    ; Repeat?
    dex
    bne loop

; Success, set carry.
done:
    sec
    jmp return

; Failure, clear carry.
fail:
    clc

; Restore everything.
return:
    ; Restore the cell coordinates.
    pla
    sta CELL_Y
    pla
    sta CELL_X

    ; Restore Y.
    pla
    tay

    ; Restore X.
    pla
    tax

    ; Restore A.
    pla
    rts

; Check the neighbors of a cell.
; Carry clear on failure.
check_neighbors:
    ; Preserve the original coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

check_0: ; Check -1, -1
    dec CELL_X
    dec CELL_Y

    ; Valid coords or skip?
    jsr check_coords
    bcc check_1

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_1: ; Check 0, -1
    inc CELL_X

    ; Valid coords or skip?
    jsr check_coords
    bcc check_2

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_2: ; Check 1, -1
    inc CELL_X

    ; Valid coords or skip?
    jsr check_coords
    bcc check_3

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_3: ; Check -1, 0
    dec CELL_X
    dec CELL_X
    inc CELL_Y

    ; Valid coords or skip?
    jsr check_coords
    bcc check_4

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_4: ; Check 1, 0 (we skip 0, 0)
    inc CELL_X
    inc CELL_X

    ; Valid coords or skip?
    jsr check_coords
    bcc check_5

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_5: ; Check -1, 1
    dec CELL_X
    dec CELL_X
    inc CELL_Y

    ; Valid coords or skip?
    jsr check_coords
    bcc check_6

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_6: ; Check 0, 1
    inc CELL_X

    ; Valid coords or skip?
    jsr check_coords
    bcc check_7

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

check_7: ; Check 1, 1
    inc CELL_X

    ; Valid coords or skip?
    jsr check_coords
    bcc check_done

    ; Test this cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; There's something here.
    bne neighbor_fail

; Check successful.
check_done:
    sec
    jmp neighbor_return

; Check failed, clear carry.
neighbor_fail:
    clc
    jmp neighbor_return

; Restore the cell coordinates.
neighbor_return:
    pla
    sta CELL_Y
    pla
    sta CELL_X
    rts
.)

; Actually place a ship at CELL_X, CELL_Y (SHIP_LENGTH, SHIP_VERTICAL, SHIP_INDEX).
place_ship:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; Preserve Y.
    tya
    pha

    ; Preserve the ship coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Write the table entry.
    ; A = SHIP_INDEX * 3
    lda SHIP_INDEX
    clc
    adc SHIP_INDEX
    clc
    adc SHIP_INDEX
    tay
    lda CELL_X
    sta (SHIP_TABLE_ADDRESS), y
    iny
    lda CELL_Y
    sta (SHIP_TABLE_ADDRESS), y
    iny
    lda SHIP_VERTICAL
    sta (SHIP_TABLE_ADDRESS), y

    ; Counter.
    ldx SHIP_LENGTH
loop:
    ; Decide what cell should be placed.
    lda SHIP_LENGTH

    ; Is this ship a short one?
    cmp #1
    beq ship_short

    ; Is this the front cell?
    cpx SHIP_LENGTH
    beq ship_front

    ; Is this the rear cell?
    cpx #1
    beq ship_rear

; Middle cell.
ship_middle:
    lda #SHIP_CELL_MIDDLE
    jmp write

; Front cell.
ship_front:
    lda #SHIP_CELL_FRONT
    jmp write

; Rear cell.
ship_rear:
    lda #SHIP_CELL_REAR
    jmp write

; A single-cell ship.
ship_short:
    lda #SHIP_CELL_SINGLE

; Write the cell.
write:
    ldy SHIP_VERTICAL
    beq skip_vertical_flag

    ; Set the vertical flag.
    clc
    adc #SHIP_CELL_VERTICAL

skip_vertical_flag:
    ; Place the ship cell.
    jsr cell_coords

    ; Do some very clever manipulation.
    ; As a result of this, SHIP_INDEX should hopefully end up at the top four bits of the ship cell.
    asl
    asl
    asl
    asl
    ora SHIP_INDEX

    ; Rotate back with RORs tweaked to produce a 8-bit rotation.
    clc
    ror
    bcc skip_ror_carry_1
    ora #$80
skip_ror_carry_1:
    clc
    ror
    bcc skip_ror_carry_2
    ora #$80
skip_ror_carry_2:
    clc
    ror
    bcc skip_ror_carry_3
    ora #$80
skip_ror_carry_3:
    clc
    ror
    bcc skip_ror_carry_4
    ora #$80
skip_ror_carry_4:

    ; Store the cell.
    sta (FIELD_ADDRESS), y

    ; Vertical orientation?
    lda SHIP_VERTICAL
    bne vertical

    ; No, horizontal.
    inc CELL_X
    jmp next

vertical:
    ; Yes, vertical.
    inc CELL_Y

next:
    ; Repeat?
    dex
    bne loop

; Restore everything.
return:
    ; Restore the cell coordinates.
    pla
    sta CELL_Y
    pla
    sta CELL_X

    ; Restore Y.
    pla
    tay

    ; Restore X.
    pla
    tax

    ; Restore A.
    pla
    rts
.)

; Surround a destroyed ship with empty cells. - CELL_X, CELL_Y (SHIP_LENGTH, SHIP_VERTICAL, SHIP_INDEX).
; The explosion sprites shall also be updated correspondingly.
surround_empty:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; Preserve Y.
    tya
    pha

    ; Preserve the ship coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Write the table entry.
    ; A = SHIP_INDEX * 3
    lda SHIP_INDEX
    clc
    adc SHIP_INDEX
    clc
    adc SHIP_INDEX
    tay
    lda CELL_X
    sta (SHIP_TABLE_ADDRESS), y
    iny
    lda CELL_Y
    sta (SHIP_TABLE_ADDRESS), y
    iny
    lda SHIP_VERTICAL
    sta (SHIP_TABLE_ADDRESS), y

    ; Counter.
    ldx SHIP_LENGTH
loop:
    ; Fill the neighbors of this cell with wreckage.
    jsr fill_neighbors

    ; Update the explosion sprites.
    jsr place_explosion

    ; Vertical orientation?
    lda SHIP_VERTICAL
    bne vertical

    ; No, horizontal.
    inc CELL_X
    jmp next

vertical:
    ; Yes, vertical.
    inc CELL_Y

next:
    ; Repeat?
    dex
    bne loop

; Restore everything.
return:
    ; Restore the cell coordinates.
    pla
    sta CELL_Y
    pla
    sta CELL_X

    ; Restore Y.
    pla
    tay

    ; Restore X.
    pla
    tax

    ; Restore A.
    pla
    rts

fill_neighbors:
    ; Preserve the cell coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Fill -1, -1
fill_0:
    dec CELL_X
    dec CELL_Y

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_1

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_1

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill 0, -1
fill_1:
    inc CELL_X

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_2

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_2

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill 1, -1
fill_2:
    inc CELL_X

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_3

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_3

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill -1, 0
fill_3:
    dec CELL_X
    dec CELL_X
    inc CELL_Y

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_4

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_4

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill 1, 0
fill_4:
    inc CELL_X
    inc CELL_X

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_5

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_5

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill -1, 1
fill_5:
    dec CELL_X
    dec CELL_X
    inc CELL_Y

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_6

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_6

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill 0, 1
fill_6:
    inc CELL_X

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_7

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_7

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

    ; Fill 1, 1
fill_7:
    inc CELL_X

    ; Verify the coordinates, skip if invalid.
    jsr check_coords
    bcc fill_done

    ; Update the cell.
    jsr cell_coords

    ; Verify emptiness.
    lda (FIELD_ADDRESS), y
    bne fill_done

    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y
    jsr update_cell

fill_done:
    ; Restore the coordinates.
    pla
    sta CELL_Y
    pla
    sta CELL_X
    rts

; Place an explosion sprite at CELL_X, CELL_Y.
place_explosion:
    ; Find the offset into the OAM (multiply X, the ship cell index, by 4).
    txa

    ; Subtract the offset (since the count is started from SHIP_LENGTH).
    sec
    sbc #1

    ; Multiply, store in Y.
    asl
    asl
    tay

    ; Skip the right offset if it's not the player's turn.
    lda PLAYER_TURN
    beq add_4
    lda #17
    jmp add_x
add_4:
    lda #4
add_x:
    ; Add the cell X coordinate.
    clc
    adc CELL_X

    ; Multiply by eight.
    asl
    asl
    asl

    ; Store into the X coordinate of the sprite.
    sta EXPLOSION_SPRITE_A + OAM_X, y

    ; Do the same with the Y coordinate...
    lda CELL_Y

    ; Y offset is 5.
    clc
    adc #5

    ; Multiply by eight.
    asl
    asl
    asl

    ; And subtract 1.
    sec
    sbc #1

    ; Store.
    sta EXPLOSION_SPRITE_A + OAM_Y, y

    ; Use the explosion tile.
    lda #TILE_EXPLOSION
    sta EXPLOSION_SPRITE_A + OAM_TILE_INDEX, y
    rts
.)

; Generates a random bit.
random_bit:
.(
    ; Preserve A.
    pha

    ; Shift the LFSR.
    asl RANDOM_LFSR
    rol RANDOM_LFSR+1
    rol RANDOM_LFSR+2

    ; No carry? Return.
    bcc return

    ; Carry was produced, so XOR our polynomial into the value.
    ; The polynomial is x^24 + x^23 + x^22 + x^17 + 1, 0x1820001.
    lda RANDOM_LFSR
    eor #$01
    sta RANDOM_LFSR
    lda RANDOM_LFSR+2
    eor #$82
    sta RANDOM_LFSR+2

    ; Set carry
    sec
return:
    ; Restore A.
    pla

    ; Return.
    rts
.)

; Generates a random X-bit integer in A.
; X is preserved.
random_bits:
.(
    ; Preserve X.
    stx TEMP_A

    ; Clear A.
    lda #$00

loop:
    ; Generate a bit.
    jsr random_bit

    ; Shift it in.
    rol

    ; Decrement X.
    dex

    ; More?
    bne loop

return:
    ; Restore X.
    ldx TEMP_A

    rts
.)

; Select the player's field.
select_player_field:
    pha

    lda #FIELD_PLAYER&$ff
    sta FIELD_ADDRESS
    lda #FIELD_PLAYER>>8
    sta FIELD_ADDRESS+1

    lda #SHIP_TABLE_PLAYER&$ff
    sta SHIP_TABLE_ADDRESS
    lda #SHIP_TABLE_PLAYER>>8
    sta SHIP_TABLE_ADDRESS+1

    pla
    rts

; Select the computer's field.
select_computer_field:
    pha

    lda #FIELD_COMPUTER&$ff
    sta FIELD_ADDRESS
    lda #FIELD_COMPUTER>>8
    sta FIELD_ADDRESS+1

    lda #SHIP_TABLE_COMPUTER&$ff
    sta SHIP_TABLE_ADDRESS
    lda #SHIP_TABLE_COMPUTER>>8
    sta SHIP_TABLE_ADDRESS+1

    pla
    rts

; Clear a field.
clear_field:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; 10x10 grid, 100 cells.
    ldy #99

    ; Empty cell.
    lda #0
loop:
    ; Store the cell.
    sta (FIELD_ADDRESS), y

    ; Decrement Y.
    dey

    ; Zero is still an iteration.
    bpl loop

return:
    ; Restore X.
    pla
    tax

    ; Restore A.
    pla

    ; Return.
    rts
.)

; Populate the selected field with ships.
; It'll be cleared beforehand.
; The cell coordinates are destroyed.
populate_field:
.(
    ; Preserve A, X and Y.
    pha
    txa
    pha
    tya
    pha

    ; Get rid of what was there beforehand.
    jsr clear_field

    ; Zero X.
    ldx #0
place:
    ; Which ship do we want?
    lda ship_lengths, x
    sta SHIP_LENGTH

    ; No more ships.
    beq return

    ; Preserve X.
    txa
    pha

    ; Attempt placing a ship...
retry:
    ; Get two random 4-bit numbers for the cell coordinates.
    ldx #4
    jsr random_bits
    sta CELL_X
    jsr random_bits
    sta CELL_Y

    ; And choose a random orientation for the ship as well.
    ldx #1
    jsr random_bits
    sta SHIP_VERTICAL

    ; Can we place a ship here?
    jsr can_place_ship
    bcc retry

    ; Update the ship index variable that place_ship uses for metadata.
    pla
    sta SHIP_INDEX
    pha

    ; Yes, place ship.
    jsr place_ship

placed:
    ; Restore X.
    pla
    tax

    ; Next entry.
    inx
    jmp place

return:
    ; Restore A, X and Y.
    pla
    tay
    pla
    tax
    pla
    rts
.)

; Lengths of ships.
;ship_lengths: .byt 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 0
ship_lengths:  .byt 4, 3, 3, 2, 2, 2, 1, 1, 1, 1, 0

; Verify whether a ship at CELL_X, CELL_Y, SHIP_LENGTH, SHIP_VERTICAL is fully destroyed.
; Returns carry if that's the case.
verify_ship:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; Preserve Y.
    tya
    pha

    ; Preserve the ship coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Counter.
    ldx SHIP_LENGTH
loop:
    ; Place the ship cell.
    jsr cell_coords
    lda (FIELD_ADDRESS), y

    ; Destroyed?
    cmp #SHIP_CELL_WRECKAGE
    bne intact_cell

    ; Vertical orientation?
    lda SHIP_VERTICAL
    bne vertical

    ; No, horizontal.
    inc CELL_X
    jmp next

vertical:
    ; Yes, vertical.
    inc CELL_Y

next:
    ; Repeat?
    dex
    bne loop

; The ship is destroyed, set carry.
destroyed:
    sec
    jmp return

; There's still a cell intact.
intact_cell:
    clc

; Restore everything.
return:
    ; Restore the cell coordinates.
    pla
    sta CELL_Y
    pla
    sta CELL_X

    ; Restore Y.
    pla
    tay

    ; Restore X.
    pla
    tax

    ; Restore A.
    pla
    rts
.)

; Shows a ship at SHIP_INDEX.
show_ship:
.(
    ; Preserve A.
    pha

    ; Preserve X.
    txa
    pha

    ; Preserve Y.
    tya
    pha

    ; Preserve the ship coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha
    lda SHIP_LENGTH
    pha
    lda SHIP_VERTICAL
    pha

    ; Retrieve the ship entry.
    ldx SHIP_INDEX
    lda ship_lengths, x
    sta SHIP_LENGTH

    ; SHIP_INDEX * 3
    lda SHIP_INDEX
    clc
    adc SHIP_INDEX
    clc
    adc SHIP_INDEX
    tay

    ; Fetch the entry.
    lda (SHIP_TABLE_ADDRESS), y
    sta CELL_X
    iny
    lda (SHIP_TABLE_ADDRESS), y
    sta CELL_Y
    iny
    lda (SHIP_TABLE_ADDRESS), y
    sta SHIP_VERTICAL

    ; Counter.
    ldx SHIP_LENGTH
loop:
    ; Update the field.
    jsr update_cell

    ; Vertical orientation?
    lda SHIP_VERTICAL
    bne vertical

    ; No, horizontal.
    inc CELL_X
    jmp next

vertical:
    ; Yes, vertical.
    inc CELL_Y

next:
    ; Repeat?
    dex
    bne loop

; Restore everything.
return:
    ; Restore the cell coordinates.
    pla
    sta SHIP_VERTICAL
    pla
    sta SHIP_LENGTH
    pla
    sta CELL_Y
    pla
    sta CELL_X

    ; Restore Y.
    pla
    tay

    ; Restore X.
    pla
    tax

    ; Restore A.
    pla
    rts
.)

; Shoot into the target field at CELL_X, CELL_Y.
; Returns with carry clear if there's been an error.
; Otherwise, X will be zero if there's no hit,
;  1 if there's a hit and 2 if there's a kill.
shoot:
.(
    ; Preserve A and Y.
    pha
    tya
    pha

    ; Preserve the cell coordinates.
    lda CELL_X
    pha
    lda CELL_Y
    pha
    lda SHIP_LENGTH
    pha
    lda SHIP_VERTICAL
    pha
    lda SHIP_INDEX
    pha

    ; Get the index into the field.
    jsr cell_coords

    ; Read the target cell and check for several conditions.
    lda (FIELD_ADDRESS), y
    bne skip_empty
    jmp empty

skip_empty:
    ; Nah, can't shoot at a cell that's already been revealed.
    cmp #SHIP_CELL_EMPTY_REVEALED
    beq error_branch_extend
    cmp #SHIP_CELL_WRECKAGE
    beq error_branch_extend

    ; Skip over.
    jmp skip_branch

; Extend the branch so that it's in range.
error_branch_extend:
    jmp error

skip_branch:
    ; If we're here, that means we've hit a ship.
    ; This is wonderful, pull out the index into the ship table.
    lda (FIELD_ADDRESS), y

    ; Preserve A, as we overwrite the cell after the index is fetched.
    pha

    ; Place a wreckage cell.
    lda #SHIP_CELL_WRECKAGE
    sta (FIELD_ADDRESS), y

    ; Update the display.
    jsr update_cell

    ; Restore A.
    pla

    ; Shift the index bits to the lowest position.
    clc
    ror
    clc
    ror
    clc
    ror
    clc
    ror

    ; And multiply by three.
    sta SHIP_INDEX
    clc
    adc SHIP_INDEX
    clc
    adc SHIP_INDEX
    tay

    ; Fetch the entry.
    lda (SHIP_TABLE_ADDRESS), y
    sta CELL_X
    iny
    lda (SHIP_TABLE_ADDRESS), y
    sta CELL_Y
    iny
    lda (SHIP_TABLE_ADDRESS), y
    sta SHIP_VERTICAL

    ; Fetch the ship length.
    lda SHIP_INDEX
    tay
    lda ship_lengths, y
    sta SHIP_LENGTH

    ; Verify the ship.
    jsr verify_ship

    ; No, still there; just a hit.
    bcc hit
kill:
    ; Play kill noise.
    jsr noise_kill

    ; Surround with revealed empty cells and show an explosion.
    jsr surround_empty

    ; And hide it.
    ldx #30
    jsr wait_vblanks
    jsr hide_explosion

    ; Increment the kill counter (opposite of who the selected field belongs to).
    lda PLAYER_TURN
    beq inc_computer
    inc PLAYER_KILLS

    ; Check win.
    lda PLAYER_KILLS
    cmp #10
    bne kill_return

    ; Set the win flag.
    lda #WIN_PLAYER
    sta WIN
    jmp kill_return
inc_computer:
    inc COMPUTER_KILLS

    ; Check win.
    lda COMPUTER_KILLS
    cmp #10
    bne kill_return

    ; Set the win flag.
    lda #WIN_COMPUTER
    sta WIN
    jmp kill_return
kill_return:
    sec
    ldx #2
    jmp return
hit:
    jsr noise_hit
    sec
    ldx #1
    jmp return
empty:
    jsr beep_empty
    ; Mark the cell as empty and revealed.
    lda #SHIP_CELL_EMPTY_REVEALED
    sta (FIELD_ADDRESS), y

    ; Update the display.
    jsr update_cell

    ; Turnaround.
    ; Is this the player's turn?
    lda PLAYER_TURN

    ; No, make it theirs now.
    beq switch_to_player

    ; Yes, thus now it's time for the computer.
    jsr turn_computer

    jmp empty_continue
switch_to_player:
    jsr turn_player
empty_continue:
    sec
    ldx #0
    jmp return
error:
    ; Error, clear carry.
    clc
return:
    ; Restore everything.
    pla
    sta SHIP_INDEX
    pla
    sta SHIP_VERTICAL
    pla
    sta SHIP_LENGTH
    pla
    sta CELL_Y
    pla
    sta CELL_X
    pla
    tay


    ; Test for win.
    lda WIN
    bne win

    ; Restore A.
    pla
    rts

win:
    ; Go to the game win screen.
    jmp win_screen
.)

; Draw the game screen.
draw_screen:
.(
    ; Clear the nametable
    jsr clear_nametable

    ; Draw the title.
    ldx #4
    ldy #3
    jsr goto_tile
    store_word(STRING_ADDRESS, caption)
    jsr draw_string

    ; Draw the notices.
    ldy #17
    jsr goto_tile
    store_word(STRING_ADDRESS, notice1)
    jsr draw_string
    ldy #18
    jsr goto_tile
    store_word(STRING_ADDRESS, notice2)
    jsr draw_string
    ldy #19
    jsr goto_tile
    store_word(STRING_ADDRESS, notice3)
    jsr draw_string

    ; Draw the fields.
    jsr select_player_field

    ; Start from the upper cell.
    lda #0
    sta FIELD_INDEX
    sta CELL_Y
player_field_row:
    ; Reset the cell X.
    lda #0
    sta CELL_X

    ; Place the cursor at the beginning of a row.
    lda CELL_Y
    adc #5
    tay

    ; Populate the empty row.
    ldx #17
    jsr goto_tile
    store_word(STRING_ADDRESS, unknown_row)
    jsr draw_string

    ; Go to the beginning of this row.
    ldx #4
    jsr goto_tile

player_field_column:
    ; Load the current cell.
    ldy FIELD_INDEX
    lda (FIELD_ADDRESS), y

    ; Apply the mask to remove the index bits.
    and #$0f

    ; And increment the index.
    iny
    sty FIELD_INDEX

    ; Draw the character.
    clc
    adc #$80
    sta PPUDATA

    ; Next column?
    inc CELL_X
    lda CELL_X
    cmp #10
    bne player_field_column

    ; Next row?
    inc CELL_Y
    lda CELL_Y
    cmp #10
    bne player_field_row

    ; Reset scrolling.
    jsr reset_scroll
    rts

caption: .byt "Player    ***  Computer", 0
unknown_row:
    .dsb 10, TILE_UNKNOWN
    .byt 0
notice1: .byt "Press A to shoot", 0
notice2: .byt "Arrows move the cursor", 0
notice3: .byt "SELECT toggles music", 0
.)

; Prompt the player for a random seed.
random_seed_prompt:
.(
    ; Clear the nametable.
    jsr clear_nametable

    ; Draw the caption.
    ldx #4
    ldy #3
    jsr goto_tile
    store_word(STRING_ADDRESS, caption1)
    jsr draw_string
    ldy #4
    jsr goto_tile
    store_word(STRING_ADDRESS, caption2)
    jsr draw_string
    ldy #7
    jsr goto_tile
    store_word(STRING_ADDRESS, caption3)
    jsr draw_string
    ldy #8
    jsr goto_tile
    store_word(STRING_ADDRESS, caption4)
    jsr draw_string

    ; Reset scrolling.
    jsr reset_scroll

    ; Enable rendering.
    jsr render_enable

    ; We need 10 keypresses.
    ldy #10
loop:

    ; Wait until pressed.
wait_press:
    jsr read_joypad
    lda #$ff
    bit TEMP_A
    beq wait_press

    ; Load the NMI counter.
    lda NMI_COUNT

    ; Make sure it's not zero.
    ora #1

    ; Store at the lowest LFSR byte.
    sta RANDOM_LFSR

    ; Shift the LFSR.
    jsr random_bit

    ; Debounce.
    ldx #10
    jsr wait_vblanks

    ; Wait until released.
wait_release:
    jsr read_joypad
    lda #$ff
    bit TEMP_A
    bne wait_release

    ; Repeat?
    dey
    bne loop

return:
    ; Disable rendering again.
    jsr render_disable
    rts

caption1: .byt "Battleship by BlueSyncLine", 0
caption2: .byt "Public domain, 2020", 0
caption3: .byt "Generating random seed", 0
caption4: .byt "Mash controller buttons", 0
.)

; Hide the explosion sprites.
hide_explosion:
    pha
    lda #0
    sta EXPLOSION_SPRITE_A + OAM_TILE_INDEX
    sta EXPLOSION_SPRITE_B + OAM_TILE_INDEX
    sta EXPLOSION_SPRITE_C + OAM_TILE_INDEX
    sta EXPLOSION_SPRITE_D + OAM_TILE_INDEX
    pla
    rts

; Display the cursor.
show_cursor:
    pha
    lda #TILE_CURSOR
    sta CURSOR_SPRITE + OAM_TILE_INDEX
    pla
    rts

; Hide the cursor.
;hide_cursor:
;    pha
;    lda #0
;    sta CURSOR_SPRITE + OAM_TILE_INDEX
;    pla
;    rts

; Update the cursor position.
update_cursor:
    pha

    ; CURSOR_X * 8
    lda CURSOR_X
    asl
    asl
    asl
    sta CURSOR_SPRITE + OAM_X

    ; CURSOR_Y * 8 - 1
    lda CURSOR_Y
    asl
    asl
    asl
    sec
    sbc #1
    sta CURSOR_SPRITE + OAM_Y

    pla
    rts

; Emit a beep.
BEEP_PERIOD = 100
BEEP_DURATION = 5
beep:
    pha
    lda #%10011010
    sta APUPULSEA_VOL
    lda #0
    sta APUPULSEA_SWEEP
    lda #BEEP_PERIOD&$ff
    sta APUPULSEA_TLOW
    lda #(BEEP_PERIOD>>8)|(BEEP_DURATION<<3)
    sta APUPULSEA_THIGH
    pla
    rts

; Emit a downwards sweeping tone.
FALL_PERIOD = 50
FALL_DURATION = 10
beep_empty:
    pha
    lda #%10010101
    sta APUPULSEA_VOL
    lda #%10010100
    sta APUPULSEA_SWEEP
    lda #FALL_PERIOD&$ff
    sta APUPULSEA_TLOW
    lda #(FALL_PERIOD>>8)|(FALL_DURATION<<3)
    sta APUPULSEA_THIGH
    pla
    rts

; Hit noise.
HIT_NOISE_PERIOD = 2
HIT_NOISE_DURATION = 3
noise_hit:
    pha
    lda #$1f
    sta APUNOISE_VOL
    lda #HIT_NOISE_PERIOD
    sta APUNOISE_PERIOD
    lda #HIT_NOISE_DURATION<<3
    sta APUNOISE_LEN
    pla
    rts

; Kill noise.
KILL_NOISE_PERIOD = 5
KILL_NOISE_DURATION = 10
noise_kill:
    pha
    lda #$1f
    sta APUNOISE_VOL
    lda #KILL_NOISE_PERIOD
    sta APUNOISE_PERIOD
    lda #KILL_NOISE_DURATION<<3
    sta APUNOISE_LEN
    pla
    rts


; Update the field cell at CELL_X, CELL_Y.
; The player's starts at Y=5, X=4, the computer's at X=17.
update_cell:
.(
    ; Preserve A, X and Y.
    pha
    txa
    pha
    tya
    pha

    ; Retrieve the Y index.
    jsr cell_coords

    ; Load the cell at that index.
    lda (FIELD_ADDRESS), y

    ; Remove the index bits.
    and #$0f

    ; Preserve A.
    pha

    ; Wait for a blanking.
    jsr wait_vblank

    ; Goto the relevant tile.
    lda CELL_X
    clc
    adc #4
    ldx PLAYER_TURN
    beq skip_increment

    ; Add 13 to the X if this is at the right (computer's) field.
    clc
    adc #13
skip_increment:
    ; Go to the tile.
    tax
    lda CELL_Y
    clc
    adc #5
    tay
    jsr goto_tile

    ; Restore A and write it as a tile.
    pla
    clc
    adc #$80
    sta PPUDATA

    ; This will fuck up the PPU's scrolling, so reset it.
    jsr reset_scroll

    ; Restore everything.
return:
    pla
    tay
    pla
    tax
    pla
    rts
.)

; Prompt the player for their move.
player_prompt_shoot:
.(
    ; Preserve CELL_X and CELL_Y for our move which will follow.
    lda CELL_X
    pha
    lda CELL_Y
    pha

    ; Recall the player's previous coordinates.
    lda PLAYER_PREV_X
    sta CELL_X
    lda PLAYER_PREV_Y
    sta CELL_Y

loop:
    ; Update the cursor position.
    lda CELL_X
    clc
    adc #17
    sta CURSOR_X
    lda CELL_Y
    clc
    adc #5
    sta CURSOR_Y
    jsr update_cursor

    ; Wait before reading the joypad.
    ldx #8
    jsr wait_vblanks

    ; Read the joypad.
    jsr read_joypad

    ; Which button is it?
    lda #JOYBTN_UP
    bit TEMP_A
    bne button_up
    lda #JOYBTN_DOWN
    bit TEMP_A
    bne button_down
    lda #JOYBTN_LEFT
    bit TEMP_A
    bne button_left
    lda #JOYBTN_RIGHT
    bit TEMP_A
    bne button_right
    lda #JOYBTN_A
    bit TEMP_A
    bne done
    lda #JOYBTN_SELECT
    bit TEMP_A
    bne button_select
    jmp loop

; Up, Y -= 1
button_up:
    lda CELL_Y
    beq loop
    dec CELL_Y
    jsr beep
    jmp loop

; Down, Y += 1
button_down:
    lda CELL_Y
    cmp #9
    beq loop
    inc CELL_Y
    jsr beep
    jmp loop

; Left, X -= 1
button_left:
    lda CELL_X
    beq loop
    dec CELL_X
    jsr beep
    jmp loop

; Right, X += 1
button_right:
    lda CELL_X
    cmp #9
    beq loop
    inc CELL_X
    jsr beep
    jmp loop

; Select, toggle music.
button_select:
    ; Toggle.
    lda MELODY_ENABLED
    bne disable_music
enable_music:
    jsr melody_enable
    jmp select_continue
disable_music:
    jsr melody_disable
select_continue:
    jsr beep

    ; Wait to avoid repeated triggering.
    ldx #20
    jsr wait_vblanks
    jmp loop

; Done.
done:
    ; Reseed the LFSR.
    lda NMI_COUNT

    ; Prevent A from being zero, since otherwise this could generate a value that'd latch the LFSR up.
    ora #1

    ; Store at the lowest byte.
    sta RANDOM_LFSR

    ; Store the coordinates chosen by the player.
    lda CELL_X
    sta PLAYER_PREV_X
    lda CELL_Y
    sta PLAYER_PREV_Y

    ; We can shoot.
    jsr shoot

    ; Error, repeat.
    bcc repeat

    ; Wait some time before returning.
wait:
    ldx #20
    jsr wait_vblanks

return:
    ; Restore the coordinates for the move.
    pla
    sta CELL_Y
    pla
    sta CELL_X
    rts

repeat:
    jmp loop
.)

; Shoot after the player is done.
shoot_turnover:
.(
    ; Preserve A.
    pha

check:
    ; Is it the player's turn?
    lda PLAYER_TURN
    beq continue

    ; Yes, allow them to shoot.
    jsr player_prompt_shoot
    jmp check

    ; We can shoot now!
continue:
    ; Place the cursor to imitate shooting.
    lda CELL_X
    clc
    adc #4
    sta CURSOR_X
    lda CELL_Y
    clc
    adc #5
    sta CURSOR_Y
    jsr update_cursor

    ; Wait for several vblanks.
    ldx #30
    jsr wait_vblanks

    ; Shoot.
    jsr shoot

    ; Done.
return:
    pla
    rts
.)

; Player's turn.
turn_player:
    pha
    lda #1
    sta PLAYER_TURN
    jsr select_computer_field
    pla
    rts

; Computer's turn.
turn_computer:
    pha
    lda #0
    sta PLAYER_TURN
    jsr select_player_field
    pla
    rts

; AI game loop.
ai_loop:
.(
ai_start:
generate_coords:
    ldx #4
    jsr random_bits
    sta CELL_X
    ldx #4
    jsr random_bits
    sta CELL_Y

    ; Are those coordinates valid?
    jsr check_coords
    bcc generate_coords

    ; Make sure the cell hasn't been seen before.
    ; We could shoot to check, but that'd incur delays as the algorithm attempts finding a suitable cell.
    jsr cell_coords
    lda FIELD_PLAYER, y
    cmp #SHIP_CELL_EMPTY_REVEALED
    beq generate_coords
    cmp #SHIP_CELL_WRECKAGE
    beq generate_coords

    ; Try shooting at it.
    jsr shoot_turnover

    ; This can't be an invalid cell, since both cases should be handled above.
assert_1:
    bcc assert_1

    ; Empty, try again.
    cpx #0
    beq generate_coords

    ; Kill, same.
    cpx #2
    beq ai_start
hit_first:

check_left:
    ; Store the alternative coordinate for going right.
    lda CELL_X
    clc
    adc #1
    sta AI_ALTERNATIVE_COORD

go_left:
    ; Decrement X.
    dec CELL_X

    ; If that would be an invalid coordinate, the ship is to the right.
    jsr check_coords
    bcc try_right

    ; Try shooting.
    jsr shoot_turnover

    ; Empty, go right.
    bcc try_right
    cpx #0
    beq try_right

    ; Hit, proceed to the left.
    cpx #1
    beq go_left

    ; Kill, go to the beginning.
    jmp ai_start

; Try going to the right.
try_right:
    lda AI_ALTERNATIVE_COORD
    sta CELL_X

right_loop:
    ; Check if we can go right.
    jsr check_coords

    ; Nope, proceed upwards.
    bcc proceed_up

    ; Shoot.
    jsr shoot_turnover

    ; The only reason we might encounter an empty cell is that going left didn't succeed, either.
    bcc proceed_up
    cpx #0
    beq proceed_up

    ; Kill, start over.
    cpx #2
    beq ai_start

    ; Increment the X-coordinate.
    inc CELL_X
    jmp right_loop

proceed_up:
    ; Return to what's hopefully the original X coordinate.
    dec CELL_X

check_up:
    ; Store the alternative coordinate for going down.
    lda CELL_Y
    clc
    adc #1
    sta AI_ALTERNATIVE_COORD

go_up:
    ; Decrement Y.
    dec CELL_Y

    ; If that would be an invalid coordinate, the ship is downwards.
    jsr check_coords
    bcc try_down

    ; Try shooting.
    jsr shoot_turnover

    ; Empty, go down.
    bcc try_down
    cpx #0
    beq try_down

    ; Hit, proceed up.
    cpx #1
    beq go_up

    ; Kill, go to the beginning.
    jmp ai_start

; Try going down, which will succeed now.
try_down:
    lda AI_ALTERNATIVE_COORD
    sta CELL_Y

down_loop:
    ; Shoot.
    jsr shoot_turnover

    ; Assertion that this will never result in an error.
assert_fail_error:
    bcc assert_fail_error

    ; Kill, start over.
    cpx #2
    beq start_over

    ; We've tried left and right, so if going up or down didn't succeed, we've failed an assertion.
    ; Hopefully an infinite loop will point this out...
assert_fail_empty:
    cpx #0
    beq assert_fail_empty

    ; Increment the Y-coordinate.
    inc CELL_Y
    jmp down_loop

; Branch target (since the label is too far away).
start_over:
    jmp ai_start
.)

; The winning screen.
win_screen:
.(
    ; Reset the stack (since this is called from a subroutine).
    ldx #$ff
    txs

    ; Show the ships if the computer won.
    lda WIN
    cmp #WIN_COMPUTER
    bne skip_show

    ; So that the right field is displayed.
    jsr turn_player
    ldx #0
show_loop:
    stx SHIP_INDEX
    jsr show_ship
    inx
    cpx #10
    bne show_loop
skip_show:
    ; We can only draw while the PPU is inactive.
    jsr wait_vblank

    ; Go to the location we'll be using for the string.
    ldx #4
    ldy #21
    jsr goto_tile

    ; Store the address.
    store_word(STRING_ADDRESS, win_player)
    lda WIN
    cmp #WIN_COMPUTER
    bne skip
    store_word(STRING_ADDRESS, win_computer)
skip:
    jsr draw_string

    ldx #4
    ldy #22
    jsr goto_tile
    store_word(STRING_ADDRESS, press_start)
    jsr draw_string
    jsr reset_scroll


    ; Wait for the player to press Start...
wait_button:
    jsr read_joypad
    lda #JOYBTN_START
    bit TEMP_A
    bne restart
    jmp wait_button

restart:
    jmp reset_game

win_player: .byt "You won!", 0
win_computer: .byt "The computer won.", 0
press_start: .byt "Press START to continue.", 0
.)


; Melody step.
melody_frame:
.(
    ; Preserve X.
    txa
    pha

    ; Check if we're playing music.
    lda MELODY_ENABLED
    beq return

    ; Time for the next note?
    lda MELODY_TIMER
    beq next_note

    ; No, decrement the counter and return.
    dec MELODY_TIMER
    jmp return

next_note:
    ; Load the index.
    ldx MELODY_INDEX

    ; Load the note.
    lda melody, x
    inc MELODY_INDEX

    ; Terminator, start over.
    cmp #$ff
    beq start_over

    ; Index -> X
    tax

    ; Load the period into the timer.
    lda period_hi, x
    sta APUTRNG_THIGH
    lda period_lo, x
    sta APUTRNG_TLOW

    ; Load the duration and store it in the timer.
    ldx MELODY_INDEX
    lda melody, x
    inc MELODY_INDEX
    sta MELODY_TIMER

    ; Return, restore X.
return:
    pla
    tax
    rts

    ; Reset everything.
start_over:
    lda #0
    sta MELODY_TIMER
    sta MELODY_INDEX
    jmp return

period_lo: .byt 0, 171, 147, 124, 103, 82, 63, 45, 28, 12, 253, 239, 225, 213, 201, 189, 179, 169, 159, 150, 142, 134, 126, 119, 112, 106, 100, 94, 89, 84, 79, 75, 70, 66, 63, 59, 56
period_hi: .byt 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

melody:
.byt 8, 30, 13, 30, 16, 30, 15, 30, 16, 30, 15, 30, 13, 30, 13, 30
.byt 13, 30, 18, 30, 15, 30, 15, 30, 16, 30, 15, 30, 12, 30, 9, 30
.byt 8, 30, 13, 60, 16, 60, 15, 30, 16, 30, 15, 30, 13, 30, 13, 30
.byt 13, 30, 18, 30, 15, 30, 15, 30, 16, 30, 15, 30, 12, 30, 8, 30
.byt 6, 60, 9, 30, 13, 30, 12, 30, 13, 30, 15, 30, 13, 30, 16, 30
.byt 18, 30, 16, 30, 11, 30, 15, 30, 15, 30, 16, 30, 15, 30, 12, 30
.byt 8, 30, 13, 60, 16, 60, 15, 30, 16, 30, 15, 30, 13, 30, 13, 30
.byt 13, 30, 18, 30, 15, 30, 15, 30, 16, 30, 15, 30, 12, 30, 9, 30
.byt 8, 30, 13, 60, 16, 60, 15, 30, 16, 30, 15, 30, 13, 30, 13, 30
.byt 13, 30, 18, 30, 20, 30, 20, 30, 21, 30, 20, 30, 18, 30, 15, 30
.byt 16, 60, 13, 30, 16, 30, 18, 30, 20, 30, 21, 30, 23, 30, 21, 30
.byt 16, 30, 20, 30, 20, 30, 21, 30, 20, 30, 18, 30, 20, 30, 28, 30
.byt 25, 30, 28, 30, 27, 30, 30, 60, 27, 30, 27, 30, 28, 30, 27, 30
.byt 24, 30, 20, 30, 25, 30, 28, 30, 27, 30, 28, 30, 27, 30, 25, 30
.byt 30, 60, 32, 30, 32, 30, 33, 30, 32, 30, 30, 30, 27, 30, 25, 120
.byt 0, 120, 255

.)

; Disable music.
melody_disable:
    pha
    lda #$80
    sta APUTRNG_BASE
    lda #0
    sta MELODY_ENABLED
    pla
    rts

; Enable music.
melody_enable:
    pha
    lda #$81
    sta APUTRNG_BASE
    lda #1
    sta MELODY_ENABLED
    pla
    rts
